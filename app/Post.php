<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title', 'description', 'content', 'author_id', 'tags',
    ];

    protected $casts = [
        'title' => 'string',
        'description' => 'string',
        'content' => 'string',
        'author_id' => 'integer',
        'tags' => 'string',
    ];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function getTags()
    {
        return explode(',', $this->tags);
    }
}
