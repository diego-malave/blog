<?php

namespace App\Http\Controllers;

use App\Post;
use App\Comment;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Http\Requests\CommentRequest;
use Illuminate\Support\MessageBag;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $posts = Post::orderBy('id', 'desc')->get();

        return view('home', compact('posts'));
    }

    public function listPost()
    {
        $posts = Post::orderBy('id', 'desc')->get();

        return view('posts.index', compact('posts'));
    }

    public function showPost(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    public function createPost()
    {
        return view('posts.create');
    }

    public function storePost(PostRequest $request)
    {
        $user = auth()->user();

        try {
            $post = new Post();
            $post->title = $request->get('title');
            $post->description = $request->get('description');
            $post->content = $request->get('content');
            $post->tags = implode(',', $request->get('tag'));
            $post->author_id = $user->id;
            $post->save();
            
            $messageBag = new MessageBag(['success' => 'A new post was saved correctly']);
        } catch (\Exception $exception) {
            \Log::debug('Error trying to store a post', [
                'exception' => $exception
            ]);

            $messageBag = new MessageBag(['danger' => 'Error trying to store a post']);
        }

        session()->flash('messages', $messageBag);
        return redirect()->route('post.list');
    }

    public function editPost(Post $post)
    {
        $user = auth()->user();

        if ($user->id != $post->author_id) {
            $messageBag = new MessageBag(['danger' => 'You cannot modify a post that is not your property.']);
            session()->flash('messages', $messageBag);
            return redirect()->route('post.list');
        }

        return view('posts.edit', compact('post'));
    }

    public function updatePost(Post $post, PostRequest $request)
    {
        $user = auth()->user();

        try {
            $post->title = $request->get('title');
            $post->description = $request->get('description');
            $post->content = $request->get('content');
            $post->tags = implode(',', $request->get('tag'));
            $post->author_id = $user->id;
            $post->save();
            
            $messageBag = new MessageBag(['success' => 'A post was update correctly']);
        } catch (\Exception $exception) {
            \Log::debug('Error trying to update a post', [
                'exception' => $exception
            ]);

            $messageBag = new MessageBag(['danger' => 'Error trying to update a post']);
        }

        session()->flash('messages', $messageBag);
        return redirect()->route('post.list');
    }

    public function deletePost(Post $post)
    {
        $user = auth()->user();

        try {
            if ($user->id != $post->author_id) {
                $messageBag = new MessageBag(['danger' => 'You cannot delete a post that is not your property.']);
                session()->flash('messages', $messageBag);
                return redirect()->route('post.list');
            }

            $post->delete();

            $messageBag = new MessageBag(['success' => 'A post was delete correctly']);
        } catch (\Exception $exception) {
            \Log::debug('Error trying to delete a post', [
                'exception' => $exception
            ]);

            $messageBag = new MessageBag(['danger' => 'Error trying to delete a post']);
        }

        session()->flash('messages', $messageBag);
        return redirect()->route('post.list', [$post->id]);
    }

    public function storeComment(CommentRequest $request)
    {
        $user = auth()->user();

        try {
            $comment = new Comment();
            $comment->content = $request->get('content');
            $comment->post_id = $request->get('post_id');
            $comment->author_id = $user->id;
            $comment->save();
            
            $messageBag = new MessageBag(['success' => 'A new comment was saved correctly']);
        } catch (\Exception $exception) {
            \Log::debug('Error trying to store a post', [
                'exception' => $exception
            ]);

            $messageBag = new MessageBag(['danger' => 'Error trying to store a comment']);
        }

        session()->flash('messages', $messageBag);
        return redirect()->route('post.show', [$request->get('post_id')]);
    }
}
