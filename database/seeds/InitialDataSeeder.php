<?php

use App\User;
use App\Post;
use App\Comment;
use Illuminate\Database\Seeder;

class InitialDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        Post::truncate();
        Comment::truncate();

        factory(User::class, 20)->create();
        factory(Post::class, 40)->create();
        factory(Comment::class, 200)->create();
    }
}
