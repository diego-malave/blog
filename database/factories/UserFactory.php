<?php


use App\User;
use App\Post;
use App\Comment;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
    ];
});

$factory->define(Post::class, function (Faker $faker) {
    $authors = User::limit(20)->get();
    $tags = explode(',', env('TAGS'));

    return [
        'title' => $faker->sentence(rand(5, 15), true),
        'description' => $faker->sentence(rand(15, 30), true),
        'content' => $faker->sentence(rand(60, 100), true),
        'author_id' => $faker->randomElement($authors),
        'tags' => implode(',', $faker->randomElements($tags, rand(1, 5))),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
    ];
});

$factory->define(Comment::class, function (Faker $faker) {
    $posts = Post::limit(20)->get();
    $authors = User::limit(20)->get();

    return [
        'post_id' => $faker->randomElement($posts),
        'author_id' => $faker->randomElement($authors),
        'content' => $faker->sentence(rand(20, 35), true),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
    ];
});
