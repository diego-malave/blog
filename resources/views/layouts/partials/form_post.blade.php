@csrf
<div class="form-group">
    <label for="exampleInputEmail1">Title</label>
    <input type="text" class="form-control" id="title" name="title" value="{{ (old('title')) ?: (isset($post->title) ? $post->title : null)  }}">
    @if($errors->has('title'))<span class="help-block">{{ $errors->first('title') }}</span>@endif
</div>
<div class="form-group">
    <label for="exampleInputPassword1">Description</label>
    <textarea class="form-control" id="description" name="description" rows="2">{{ (old('description')) ?: (isset($post->description) ? $post->description : null) }}</textarea>
    @if($errors->has('description'))<span class="help-block">{{ $errors->first('description')  }}</span>@endif
</div>
<div class="form-group">
    <label for="exampleInputPassword1">Content</label>
    <textarea class="form-control" id="content" name="content" rows="4">{{ (old('content')) ?: (isset($post->content) ? $post->content : null) }}</textarea>
    @if($errors->has('content'))<span class="help-block">{{ $errors->first('content') }}</span>@endif
</div>
<div class="form-check">
    @php($array_tags = explode(',', (isset($post) ? $post->tags : null)))
    @foreach(explode(',', env('TAGS')) as $key => $tag)
        <input class="form-check-input" type="checkbox" id="tag{{$key}}" name="tag[]" value="{{ $tag }}" {{ (in_array($tag, $array_tags)) ? 'checked' : null }}>
        <label class="form-check-label" for="tag_{{$key}}">{{$tag}}</label><br>
    @endforeach
</div>
@if($errors->has('tag'))<span class="help-block">{{ $errors->first('tag') }}</span>@endif
<br>
