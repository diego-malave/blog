@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>Edit Post</h1>
        <form method="POST" action="{{ route('post.update', [$post->id]) }}">
            @include('layouts.partials.form_post')
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection
