@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="col mb-4">
            <div class="card">
                <svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image cap"><title>Placeholder</title><rect width="100%" height="100%" fill="#868e96"></rect><text x="45%" y="50%" fill="#dee2e6" dy=".3em">Image Test</text></svg>
                <div class="card-body">
                    <h5 class="card-title"><a href="{{ route('post.list', [$post->id]) }}">{{ $post->title }}</a> </h5>
                    <p class="card-text">{{ $post->content }}</p>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-lg-10">
                            <small class="text-muted">
                                Date: {{ $post->created_at }} |
                                Author: {{ ucfirst($post->author->name) }} |
                                Tags: @include('layouts.partials.tags') |
                                Messages: <a href="#" class="badge-pill badge-danger">{{ $post->comments()->count() }}</a>
                            </small>
                        </div>
                        <div class="col-lg-2 text-right">
                            <a href="{{ route('post.create') }}" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">New Post</a>
                        </div>
                    </div>
                </div>
            </div>
            
            @if($post->comments()->count() > 0)
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Date</th>
                        <th scope="col">Author</th>
                        <th scope="col">Content</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($post->comments as $comment)
                            <tr>
                                <th scope="row" style="width: 5%">{{ $comment->id }}</th>
                                <td style="width: 20%">{{ $comment->created_at }}</td>
                                <td style="width: 20%">{{ $comment->author->name }}</td>
                                <td style="width: 55%">{{ $comment->content }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
            <br>
            <h1>New Comment</h1>
            <form method="POST" action="{{ route('comment.store') }}">
                <input type="hidden" name="post_id" value="{{$post->id}}">
                @csrf
                <div class="form-group">
                    <textarea class="form-control" id="content" name="content" rows="4">{{old('content')}}</textarea>
                    @if($errors->has('content'))<span class="help-block">{{ $errors->first('content') }}</span>@endif
                </div>
                <br>
                <button type="submit" class="btn btn-primary">Comment</button>
            </form>
        </div>
    </div>
@endsection
