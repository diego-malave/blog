@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>New Post</h1>
        <form method="POST" action="{{ route('post.store') }}">
            <input type="hidden" name="post_id" value="{{ (isset($post)) ? $post->id : null}}">
            @include('layouts.partials.form_post')
            <button type="submit" class="btn btn-primary">Create</button>
        </form>
    </div>
@endsection
