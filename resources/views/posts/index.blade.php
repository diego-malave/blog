@extends('layouts.app')
@section('content')
    <div class="container">
        @foreach($posts as $post)
            <div class="col mb-4">
                <div class="card">
                    <svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image cap"><title>Placeholder</title><rect width="100%" height="100%" fill="#868e96"></rect><text x="45%" y="50%" fill="#dee2e6" dy=".3em">Image Test</text></svg>
                    <div class="card-body">
                        <h5 class="card-title"><a href="{{ route('post.show', [$post->id]) }}">{{ $post->title }}</a> </h5>
                        <p class="card-text">{{ $post->description }}</p>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-9">
                                <small class="text-muted">
                                    {{ $post->created_at }} |
                                    {{ ucfirst($post->author->name) }} |
                                    @include('layouts.partials.tags') |
                                    Messages: <a href="#" class="badge-pill badge-danger">{{ $post->comments()->count() }}</a>
                                </small>
                            </div>
                            <div class="col-lg-3 text-right">
                                <a href="{{ route('post.delete',[$post->id]) }}" id="formularioAgregar" class="btn btn-danger btn-sm active" role="button" aria-pressed="true">Delete</a>
                                <a href="{{ route('post.edit',[$post->id]) }}" class="btn btn-warning btn-sm active" role="button" aria-pressed="true">Edit</a>
                                <a href="{{ route('post.create') }}" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">New</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
