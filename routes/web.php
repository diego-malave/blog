<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/post/list', 'HomeController@listPost')->name('post.list');
Route::get('/post/create', 'HomeController@createPost')->name('post.create');
Route::get('/post/{post}/edit', 'HomeController@editPost')->name('post.edit');
Route::post('/post/{post}/update', 'HomeController@updatePost')->name('post.update');
Route::get('/post/{post}/delete', 'HomeController@deletePost')->name('post.delete');
Route::get('/post/{post}', 'HomeController@showPost')->name('post.show');
Route::post('/post', 'HomeController@storePost')->name('post.store');
Route::post('/comment', 'HomeController@storeComment')->name('comment.store');
Auth::routes();
